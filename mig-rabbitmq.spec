# find the RabbitMQ version from the tarball
%define rabbitmq_url %((grep rabbitmq-server-generic-unix %{_sourcedir}/sources || \
                        grep rabbitmq-server-generic-unix %{_builddir}/sources  || \
                        echo "x unknown") 2>/dev/null | perl -ane 'print("$F[1]")')
%define rabbitmq_tar %(basename %{rabbitmq_url})
%define rabbitmq_version %(echo %{rabbitmq_tar} | perl -pe 's/^rabbitmq-server-generic-unix-//; s/\\.tar\\.gz$//')

# installation settings
%define rabbitmq_share /usr/share/rabbitmq
%define rabbitmq_home  /usr/share/rabbitmq-%{rabbitmq_version}

Summary:	RabitMQ Messaging Broker
Name:		mig-rabbitmq
Version:	%{rabbitmq_version}
Release:	1%{?dist}
License:	MPLv1.1
Group:		System
Source0:	%{rabbitmq_tar}
Source1:	rabbitmqadmin
Source2:	rabbitmq-server.init
Source3:	rabbitmq-env
Source4:	sources
URL: 		http://www.rabbitmq.com/
Requires:	erlang-kernel >= R14B
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires:	perl
BuildArch:	noarch

%description
RabbitMQ is an implementation of AMQP, the emerging standard for high
performance enterprise messaging. The RabbitMQ server is a robust and
scalable implementation of an AMQP broker.

%prep
%setup -q -n rabbitmq_server-%{rabbitmq_version}

%build
# cleanup the docs
rm -f INSTALL
mkdir -p share/doc
mv LICENSE* share/doc/
# inline the version string inside rabbitmqadmin
sed -e 's/%%%%VSN%%%%/%{rabbitmq_version}/' %{SOURCE1} > rabbitmqadmin.tmp
# put rabbitmqadmin in sbin
install -m 0755 rabbitmqadmin.tmp sbin/rabbitmqadmin
# add our service script
install -m 0755 %{SOURCE2} sbin/service
# replace rabbitmq-env with our version (without hard-coded paths)
install -m 0755 %{SOURCE3} sbin/rabbitmq-env

%install
rm -fr %{buildroot}
mkdir -p %{buildroot}%{rabbitmq_home}
mv * %{buildroot}%{rabbitmq_home}

%post
[ -e %{rabbitmq_share} ] && rm -f %{rabbitmq_share}
ln -s rabbitmq-%{rabbitmq_version} %{rabbitmq_share}

%postun
rm -f %{rabbitmq_share}
cd /usr/share
last=`ls -d rabbitmq-* 2>/dev/null | sort | tail -1`
if [ "x$last" != "x" ]; then
  ln -s $last %{rabbitmq_share}
fi

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root,-)
%{rabbitmq_home}
